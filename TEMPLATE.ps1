######################
# [module name]
######################
<#

.SYNOPSIS
[intention]

.DESCRIPTION
[description, set software purpose steps]
1.[such as: get info]
2.[such as: set info]
3.[such as: report info]

.INPUTS
[input raw data]

.OUTPUTS
[output processed data]

.EXAMPLE
[demo]

.NOTES
Author    : Raul van der Raaf
Created   : 11-2021
Version	  : 1.0.0
Requires  : PowerShell v5.0

.ABOUT

.CHANGELOG

#>
   

######################
# SETTINGS
######################

#General Options
$ModuleName = "New Module" #required


######################
# CLASSES
######################


######################
# FUNCTIONS
######################


######################
# START RUNTIME
######################

#Set App Window Title
$HostName = $Env:ComputerName
$Version = $Host.Version
$Host.UI.RawUI.WindowTitle = “$HostName | PowerShell $Version | $ModuleName”

#RUN, FORREST, RUN!


#1.[such as: get info]


#2.[such as: set info]


#3.[such as: report info]




######################
# END RUNTIME
######################


######################
# CLEAN UP?
######################

#Clear DB Connections

#Clear Variables
#Remove-Variable * -ErrorAction SilentlyContinue

#Clear Functions
#Remove-Item Function:\CPF_* -ErrorAction SilentlyContinue

#Clear Classes